Filename = 'sydney_tiny.png'; % Input file name
blockSize = 8;                        % Block size for processing
halfBlockSize = ceil(blockSize/2);		      % Half the size of one block
Image = double(imread(Filename));
SearchWindowSize = 31;
[imageHeight,imageWidth,C] = size(Image);

%% TASK 2.2
% Detect pixels that need to be inpainted (Mask=0: inpaint, Mask=1: leave untouched)
Mask = zeros(imageHeight,imageWidth);

tol =[180 180 40];  % yellow: high red, high green, low blue

redBand = Image(:,:,1);
T1 = tol(1);
Mask(redBand < T1) = 1;

greenBand = Image(:,:,2);
T2 = tol(2);
Mask(greenBand < T2) = 1;

blueBand = Image(:,:,3);
T3 = tol(3);
Mask(blueBand > T3) = 1;

se = strel('square', 3);
Mask = imerode(Mask, se);

%InpaintedImage = Image .* Mask(:,:,ones(1,1,3));

%subplot(2,2,1),imshow(uint8(Image)); title('Original Image');
%subplot(2,2,2),imshow(Mask,[]); title('Detected Areas');
%subplot(2,2,3),imshow(uint8(InpaintedImage)); title('Masked Image');
      



% set the pixels that need to be inpainted to zero
InpaintedImage = Image .* Mask(:,:,ones(1,1,3));

% plot InpaintedImage
%figure,imagesc(uint8(InpaintedImage));


counter = 0;
total = sum(sum(Mask==0)); % total number of pixels to inpaint
left = total; % number of pixels left to inpaint
iter = 0; % iteration number

%figure;


yBand  = 0.299*Image(:,:,1) + 0.587*Image(:,:,2) + 0.114*Image(:,:,3);
rBand = InpaintedImage(:,:,1);
gBand = InpaintedImage(:,:,2);
bBand = InpaintedImage(:,:,3);

previousPixelHeight = 1;
previousPixelWidth = 1;

while left>0
    %left
    % Select one outer slice of the remaining inpainting regions  
    Slice = 1 - Mask;
    found = 0;
    pixelHeight = 0;
    pixelWidth = 0;
    if (previousPixelWidth < imageWidth && Mask(previousPixelHeight, previousPixelWidth + 1) == 0)
        pixelHeight = previousPixelHeight;
        pixelWidth = previousPixelWidth + 1;
        previousPixelWidth = pixelWidth;
    elseif (previousPixelHeight < imageHeight && Mask(previousPixelHeight + 1, previousPixelWidth) == 0)
        pixelHeight = previousPixelHeight + 1;
        pixelWidth = previousPixelWidth;
        previousPixelHeight = pixelHeight;
    elseif (previousPixelWidth > 1 && Mask(previousPixelHeight, previousPixelWidth - 1) == 0)
        pixelHeight = previousPixelHeight;
        pixelWidth = previousPixelWidth - 1;
        previousPixelWidth = pixelWidth;
    elseif (previousPixelHeight > 1 && Mask(previousPixelHeight - 1, previousPixelWidth) == 0)
        pixelHeight = previousPixelHeight - 1;
        pixelWidth = previousPixelWidth;
        previousPixelHeight = pixelHeight;
    else
        for i = 1:imageHeight,
            for j = 1:imageWidth,
                if (Mask(i,j) == 0)
                    pixelHeight = i;
                    pixelWidth = j;
                    previousPixelHeight = i;
                    previousPixelWidth = j;
                    i = imageHeight;
                    j = imageWidth;
                end;
            end;
        end;
    end;
    
    InpaintedImage_New = zeros(size(InpaintedImage));
    weights_accum = zeros(size(InpaintedImage));
    
    msdBest = -1;
    pixelBestHeight = 0;
    pixelBestWidth = 0;
    
    
    for Delta_Height = -floor(SearchWindowSize/2):floor(SearchWindowSize/2),
        for Delta_Width = -floor(SearchWindowSize/2):floor(SearchWindowSize/2),       

	    % non-zero displacement
            if (Delta_Height~=0 || Delta_Width~=0),

                % compute the feasible ranges for m and n (to stay within the image boundaries)
                blockCenterHeight = pixelHeight + Delta_Height;
                blockCenterWidth = pixelWidth + Delta_Width;
                if (blockCenterHeight < halfBlockSize + 1 || blockCenterHeight > imageHeight - halfBlockSize || blockCenterWidth < halfBlockSize + 1 || blockCenterWidth > imageWidth - halfBlockSize - 2 || (Delta_Height == 0 && Delta_Width == 0))
                    
                    continue;
                end;
                
                % check if center pixel of block is available
                if (Mask(Delta_Height + pixelHeight,Delta_Width + pixelWidth) == 0)
                    continue;
                end;
                
                msdTemp = 0;
                originalPixels = 0;
                
                for i = (-halfBlockSize+1):halfBlockSize-1,
                    if (msdTemp <= msdBest || msdBest == -1)
                        for j = -halfBlockSize+1:halfBlockSize-1,
                            if ((Mask(pixelHeight + i + Delta_Height, pixelWidth + j + Delta_Width) == 1 ) && (pixelHeight + i <= imageHeight && pixelWidth + j <= imageWidth && pixelHeight + i >=1 && pixelWidth +j >=1 && Mask(pixelHeight + i,pixelWidth + j) == 1))
                                originalPixels = originalPixels + 1;
                                msdTemp = msdTemp + abs(yBand(pixelHeight + i, pixelWidth + j) - yBand(pixelHeight + i + Delta_Height, pixelWidth + j + Delta_Width)).^2;
                                %msdTemp = msdTemp + abs(rBand(pixelHeight + i, pixelWidth + j) - rBand(pixelHeight + i + Delta_Height, pixelWidth + j + Delta_Width)).^2;
                                %msdTemp = msdTemp + abs(gBand(pixelHeight + i, pixelWidth + j) - gBand(pixelHeight + i + Delta_Height, pixelWidth + j + Delta_Width)).^2;
                                %msdTemp = msdTemp + abs(bBand(pixelHeight + i, pixelWidth + j) - bBand(pixelHeight + i + Delta_Height, pixelWidth + j + Delta_Width)).^2;
                            end;
                        end;
                    end;
                end;
                
                if (originalPixels > 0)
                    msdTemp = msdTemp*1.0/(originalPixels.^2);
                    if (msdTemp < msdBest || msdBest == -1) &&(Delta_Height >2 ||Delta_Width>2)
                        originalPixels;
                        msdBest = msdTemp;
                        pixelBestHeight = pixelHeight + Delta_Height;
                        pixelBestWidth = pixelWidth + Delta_Width;
                    end;
                end;    
            end;
        end;
    end;
    if(pixelBestHeight ~=0 && pixelBestWidth ~= 0)
        Mask(pixelHeight,pixelWidth) = 2;
        InpaintedImage(pixelHeight,pixelWidth,:) = InpaintedImage(pixelBestHeight,pixelBestWidth,:);
        blockSize = 8;                        % Block size for processing
        halfBlockSize = ceil(blockSize/2);		      % Half the size of one block
        SearchWindowSize = 31;
        %fprintf('fixed \n')
    else
        %fprintf('broken');
        blockSize = floor(1.5*blockSize);                        % Block size for processing
        halfBlockSize = floor(1.5*halfBlockSize);		      % Half the size of one block
        SearchWindowSize = floor(1.5*SearchWindowSize);
    end;
    % TO DO: update the mask according to the pixels that were inpainted in the last
    %        iteration.


    % compute the number of pixels left
    left=sum(sum(Mask==0));
    
    %subplot(121),imagesc(uint8(Image)),title('Degraded image');
    %subplot(122),imagesc(uint8(InpaintedImage)),title('Inpainted image');
    %pause(0.1);
    if(mod(left,100) ==  0)
        subplot(121),imagesc(uint8(Image)),title('Degraded image');
        subplot(122),imagesc(uint8(InpaintedImage)),title('Inpainted image');
        pause(0.1);
    end;
    % go to the next iteration
    
end;