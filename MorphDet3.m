clear
Filename = 'ww1_original.png';      % original degraded sample image
FileMasked = 'ww1_mask.png';        % masked sample image
Image = double(imread(Filename));   
[M,N,C] = size(Image);

yBand  = 0.299*Image(:,:,1) + 0.587*Image(:,:,2) + 0.114*Image(:,:,3);
startImage = yBand;

% morphological closing
se = strel('line', 6, 10);  % structural element
morphImage = imdilate(startImage, se);
morphImage= imerode(morphImage, se);

subImage = (morphImage-startImage); 

% denoising median filter of substracted image
filtImage = medfilt2(subImage, [2 2], 'symmetric');

%thresholding
thresh = 20;
threshImage = ones(M,N);
threshImage(filtImage > thresh) = 0;

% erode mask
se = strel('square',10);
erodImage = imerode(threshImage,se);

% compare with neighbourhood average
l=10;
nb=2;
extImage = wextend('2D','sym',startImage,l);
threshImage = ones(M,N);
for i= 1:M,
    for j= 1:N,
        if erodImage(i,j) == 0
            block = extImage(i:i+2*l, j:j+2*l);
            neighb = extImage(i+l-nb:i+l+nb, j+l-nb:j+l+nb);
            avg = mean(mean(block));
            sigma = std(std(block));
            avgnb = mean(mean(neighb));
            if avgnb < avg-sigma
                threshImage(i,j) = 0;
            end
        end
    end
end

maskImage = threshImage;

% apply yellow mask on degraded image
RImage = Image(:,:,1); GImage = Image(:,:,2); BImage = Image(:,:,3); 
RImage(maskImage == 0) = 240;
GImage(maskImage == 0) = 240;
BImage(maskImage == 0) = 40;
maskedImage(:,:,1) = RImage; maskedImage(:,:,2) = GImage; maskedImage(:,:,3) = BImage; 


figure, subplot(1,2,1), imagesc(uint8(maskedImage)), title('Masked');
subplot(1,2,2), imagesc(imread(FileMasked)), title('Masked example');
