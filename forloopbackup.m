for i = (-halfBlockSize+1):halfBlockSize-1,
                        for j = -halfBlockSize+1:halfBlockSize-1,
                            if level ~= nLevel || Mask(pixelHeight,pixelWidth) == 3,
                               if ((Mask(pixelHeight + i + Delta_Height, pixelWidth + j + Delta_Width) == 1 ) && (pixelHeight + i <= imageHeight && pixelWidth + j <= imageWidth && pixelHeight + i >=1 && pixelWidth +j >=1))
                                  originalPixels = originalPixels + 1;
                                  msdTemp = msdTemp + sum((cA{level}(pixelHeight + i, pixelWidth + j,:) - cA{level}(pixelHeight + i + Delta_Height, pixelWidth + j + Delta_Width,:)).^2);
                                  msdTemp = msdTemp + sum((cV{level}(pixelHeight + i, pixelWidth + j,:) - cV{level}(pixelHeight + i + Delta_Height, pixelWidth + j + Delta_Width,:)).^2);
                                  msdTemp = msdTemp + sum((cH{level}(pixelHeight + i, pixelWidth + j,:) - cH{level}(pixelHeight + i + Delta_Height, pixelWidth + j + Delta_Width,:)).^2);
                                  msdTemp = msdTemp + sum((cD{level}(pixelHeight + i, pixelWidth + j,:) - cD{level}(pixelHeight + i + Delta_Height, pixelWidth + j + Delta_Width,:)).^2);
                               end;
                            else
                                if ((Mask(pixelHeight + i + Delta_Height, pixelWidth + j + Delta_Width) == 1 ) && (pixelHeight + i <= imageHeight && pixelWidth + j <= imageWidth && pixelHeight + i >=1 && pixelWidth +j >=1 && Mask(pixelHeight + i,pixelWidth + j) == 1))
                                    originalPixels = originalPixels + 1;
                                    msdTemp = msdTemp + sum((cA{level}(pixelHeight + i, pixelWidth + j,:) - cA{level}(pixelHeight + i + Delta_Height, pixelWidth + j + Delta_Width,:)).^2);
                                    msdTemp = msdTemp + sum((cV{level}(pixelHeight + i, pixelWidth + j,:) - cV{level}(pixelHeight + i + Delta_Height, pixelWidth + j + Delta_Width,:)).^2);
                                    msdTemp = msdTemp + sum((cH{level}(pixelHeight + i, pixelWidth + j,:) - cH{level}(pixelHeight + i + Delta_Height, pixelWidth + j + Delta_Width,:)).^2);
                                    msdTemp = msdTemp + sum((cD{level}(pixelHeight + i, pixelWidth + j,:) - cD{level}(pixelHeight + i + Delta_Height, pixelWidth + j + Delta_Width,:)).^2);
                                end;
                            end;
                        end;
                    end;