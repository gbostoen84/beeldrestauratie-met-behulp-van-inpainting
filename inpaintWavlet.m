function inpaintWavlet( file, blockSize_begin, searchWindowSize_begin,nLevel,noise,thresholdNoise)
if nargin < 1
    file = input('Give file:    ', 's');
    blockSize_begin = input('blocksize[default is 8]:    ', 's');
    if isempty(blockSize_begin)
        blockSize_begin = 8;
    else
        blockSize_begin = str2num(blockSize_begin);
    end;
    searchWindowSize_begin = input('search window size[default is 31]:  ','s');
    if isempty(searchWindowSize_begin)
        searchWindowSize_begin = 31;
    else
        searchWindowSize_begin = str2num(searchWindowSize_begin);
    end
    %# Number of decompositions
    nLevel = input('levels used [default is 2]: ','s');
    if isempty(nLevel)
        nLevel = 2;
    else
        nLevel = str2num(nLevel);
    end
    noise = input('noise used [default is 0]: ','s');
    if isempty(noise)
        noise = 0;
    else
        noise = str2num(noise);
    end
    thresholdNoise = input('threshold used to remove noise [default is 0]: ','s');
    if isempty(thresholdNoise)
        thresholdNoise = 0;
    else
        thresholdNoise = str2num(thresholdNoise);
    end
end;

image = imread(file);  %# Load sample image

%add noise
for i=1:size(image,1),
    for j=1:size(image,2),
        image(i,j) = min(max(image(i,j) + randn*noise-noise/2,0),255);
    end
end

%remove noise
[LL,LH,HL,HH] = dwt2(image,'db1');
LH(abs(LH) <= thresholdNoise) = 0;
HL(abs(HL) <= thresholdNoise) = 0;
HH(abs(HH) <= thresholdNoise) = 0;
image = idwt2(LL,LH,HL,HH,'db1',size(image));


blockSize = blockSize_begin;                        % Block size for processing
halfBlockSize = ceil(blockSize/2);		      % Half the size of one block
searchWindowSize = searchWindowSize_begin;

            
cA = cell(1,nLevel);    %# Approximation coefficient storage
cH = cell(1,nLevel);    %# Horizontal detail coefficient storage
cV = cell(1,nLevel);    %# Vertical detail coefficient storage
cD = cell(1,nLevel);    %# Diagonal detail coefficient storage
cM = cell(1,nLevel);    %# Mask
msdValues = cell(1,nLevel);



%wavelet transform
startImage = image;
for iLevel = 1:nLevel,  %# Apply nLevel decompositions
    [cA{iLevel},cH{iLevel},cV{iLevel},cD{iLevel}] = dwt2(startImage,'db1');
    startImage = cA{iLevel};
end


%mask creation
tol =[180 180 40];  % yellow: high red, high green, low blue
imageTest = uint8(wcodemat(cA{1},256));
[imageHeight,imageWidth,C] = size(imageTest);
mask = zeros(imageHeight,imageWidth);

redBand = imageTest(:,:,1);
T1 = tol(1);
mask(redBand < T1) = 1;

greenBand = imageTest(:,:,2);
T2 = tol(2);
mask(greenBand < T2) = 1;

blueBand = imageTest(:,:,3);
T3 = tol(3);
mask(blueBand > T3) = 1;

se = strel('square', 3);
mask = imerode(mask, se);

%this is the masker used for level 1
cM{1} = mask;

% resize masks for higher levels 
for iLevel=1:nLevel,
    cM{iLevel} = round(imresize(cM{1},size(cA{iLevel},1)./size(cA{1},1)));
end;

for level=nLevel:-1:1
    cA_repaint = cA{level};
    cV_repaint = cV{level};
    cH_repaint = cH{level};
    cD_repaint = cD{level};
    mask = cM{level};
    
    total = sum(sum(mask==0)) + sum(sum(mask == 3)); % total number of pixels to inpaint
    left = total; % number of pixels left to inpaint
    
    [imageHeight,imageWidth,C] = size(cA{level});
    
    msdValues{level}(1:imageHeight,1:imageWidth) = -1;
    
    %iterate until all pixels have been inpainted
    while left > 0
        pixelHeight = 0;
        pixelWidth = 0;
        size(cA{level});
        %calculate position of the pixel that needs inpainting
        for i = 1:imageHeight,
            for j = 1:imageWidth,
                if (mask(i,j) == 0)
                    pixelHeight = i;
                    pixelWidth = j;
                    i = imageHeight;
                    j = imageWidth;
                end;
            end;
        end;
        %if there's no pixel found to be inpainted stop
        if(pixelHeight ==0 || pixelWidth == 0)
            left = 0;
            continue;
        end;
        
        msdBest = -1;
        pixelBestHeight = 0;
        pixelBestWidth = 0;
        
        %calculate block around pixel
        lowestHeight = max(min(pixelHeight-halfBlockSize+1,imageHeight),1);
        highestHeight = max(min(pixelHeight+halfBlockSize,imageHeight),1);
        
        lowestWidth = max(min(pixelWidth-halfBlockSize+1,imageWidth),1);
        highestWidth = max(min(pixelWidth+halfBlockSize,imageWidth),1);
        
        cAtempOrigin = cA{level}(lowestHeight :highestHeight, lowestWidth: highestWidth);
        cVtempOrigin = cV{level}(lowestHeight :highestHeight, lowestWidth: highestWidth);
        cHtempOrigin = cH{level}(lowestHeight :highestHeight, lowestWidth: highestWidth);
        cDtempOrigin = cD{level}(lowestHeight :highestHeight, lowestWidth: highestWidth);
        if(level == nLevel)
            validBitsOrigin = mask(lowestHeight :highestHeight, lowestWidth: highestWidth) == 1;
        else
            validBitsOrigin = mask(lowestHeight :highestHeight, lowestWidth: highestWidth) > -1;
        end;
        % some fixes to get good block of correct blockSize
        if(size(validBitsOrigin,1) <blockSize),
            amount = blockSize - size(validBitsOrigin,1);
            row = zeros(amount,size(validBitsOrigin,2));
            if(lowestHeight == 1),
                validBitsOrigin = [row == 1;validBitsOrigin];
                cAtempOrigin = [row;cAtempOrigin];
                cVtempOrigin = [row;cVtempOrigin];
                cHtempOrigin = [row;cHtempOrigin];
                cDtempOrigin = [row;cDtempOrigin];
            else,
                validBitsOrigin = [validBitsOrigin;row == 1];
                cAtempOrigin = [cAtempOrigin;row];
                cVtempOrigin = [cVtempOrigin;row];
                cHtempOrigin = [cHtempOrigin;row];
                cDtempOrigin = [cDtempOrigin;row];
            end;
        end;
        if(size(validBitsOrigin,2) < blockSize),
            amount = blockSize - size(validBitsOrigin,2);
            row = zeros(blockSize,amount);
            if(lowestWidth == 1),
                validBitsOrigin = [row == 1,validBitsOrigin];
                cAtempOrigin = [row,cAtempOrigin];
                cVtempOrigin = [row,cVtempOrigin];
                cHtempOrigin = [row,cHtempOrigin];
                cDtempOrigin = [row,cDtempOrigin];
            else,
                validBitsOrigin = [validBitsOrigin,row == 1];
                cAtempOrigin = [cAtempOrigin,row];
                cVtempOrigin = [cVtempOrigin,row];
                cHtempOrigin = [cHtempOrigin,row];
                cDtempOrigin = [cDtempOrigin,row];
            end;
        end;
        if(sum(sum(validBitsOrigin)) == 0),
            blockSize = floor(1.5*blockSize);                        % Block size for processing
            halfBlockSize = floor(1.5*halfBlockSize);		      % Half the size of one block
            searchWindowSize = floor(1.5*searchWindowSize);
            continue;
        end;
        
        for Delta_Height = -floor(searchWindowSize/2):floor(searchWindowSize/2),
            for Delta_Width = -floor(searchWindowSize/2):floor(searchWindowSize/2),
                
                % non-zero displacement
                if (Delta_Height~=0 || Delta_Width~=0),
                    
                    % compute the feasible ranges for m and n (to stay within the image boundaries)
                    blockCenterHeight = pixelHeight + Delta_Height;
                    blockCenterWidth = pixelWidth + Delta_Width;
                    if (blockCenterHeight < halfBlockSize + 1 || blockCenterHeight > imageHeight - halfBlockSize || blockCenterWidth < halfBlockSize + 1 || blockCenterWidth > imageWidth - halfBlockSize - 2 || (Delta_Height == 0 && Delta_Width == 0))
                        continue;
                    end;
                    
                    % check if center pixel of block is available
                    if (mask(Delta_Height + pixelHeight,Delta_Width + pixelWidth) ~= 1)
                        continue;
                    end;
                    
                    msdTemp = 0;
                    originalPixels = 0;
                    lowestHeight = max(min(pixelHeight-halfBlockSize+Delta_Height+1,imageHeight),1);
                    highestHeight = max(min(pixelHeight+halfBlockSize+Delta_Height,imageHeight),1);
                    
                    lowestWidth = max(min(pixelWidth-halfBlockSize+Delta_Width+1,imageWidth),1);
                    highestWidth = max(min(pixelWidth+halfBlockSize+Delta_Width,imageWidth),1);
                    
                    cAtempDest = cA{level}(lowestHeight :highestHeight, lowestWidth: highestWidth);
                    cVtempDest = cV{level}(lowestHeight :highestHeight, lowestWidth: highestWidth);
                    cHtempDest = cH{level}(lowestHeight :highestHeight, lowestWidth: highestWidth);
                    cDtempDest = cD{level}(lowestHeight :highestHeight, lowestWidth: highestWidth);
                    
                    if(level == nLevel),
                        validBitsDest = mask(lowestHeight :highestHeight, lowestWidth: highestWidth) == 1;
                    else
                        validBitsDest = mask(lowestHeight :highestHeight, lowestWidth: highestWidth) >-1;
                    end;
                    
                    % some fixes to get good block
                    if(size(validBitsDest,1) <blockSize),
                        amount = blockSize - size(validBitsDest,1);
                        row = zeros(amount,size(validBitsDest,2));
                        if(lowestHeight == 1),
                            validBitsDest = [row == 1;validBitsDest];
                            cAtempDest = [row;cAtempDest];
                            cVtempDest = [row;cVtempDest];
                            cHtempDest = [row;cHtempDest];
                            cDtempDest = [row;cDtempDest];
                        else,
                            validBitsDest = [validBitsDest;row == 1];
                            cAtempDest = [cAtempDest;row];
                            cVtempDest = [cVtempDest;row];
                            cHtempDest = [cHtempDest;row];
                            cDtempDest = [cDtempDest;row];
                        end;
                    end;
                    if(size(validBitsDest,2) < blockSize),
                        amount = blockSize - size(validBitsDest,2);
                        row = zeros(blockSize,amount);
                        if(lowestWidth == 1),
                            validBitsDest = [row == 1,validBitsDest];
                            cAtempDest = [row,cAtempDest];
                            cVtempDest = [row,cVtempDest];
                            cHtempDest = [row,cHtempDest];
                            cDtempDest = [row,cDtempDest];
                        else,
                            validBitsDest = [validBitsDest,row == 1];
                            cAtempDest = [cAtempDest,row];
                            cVtempDest = [cVtempDest,row];
                            cHtempDest = [cHtempDest,row];
                            cDtempDest = [cDtempDest,row];
                        end;
                    end;
                    
                    validBits = min(validBitsDest,validBitsOrigin);
                    originalPixels = originalPixels + sum(sum(validBits));
                    msdTemp = (sum((cAtempDest(validBits)-cAtempOrigin(validBits)).^2)+sum((cVtempDest(validBits)-cVtempOrigin(validBits)).^2)+sum((cHtempDest(validBits)-cHtempOrigin(validBits)).^2)+sum((cDtempDest(validBits)-cDtempOrigin(validBits)).^2));
                    
                    if (originalPixels > 0)
                        msdTemp = msdTemp*1.0/(originalPixels.^2);
                        if (msdTemp < msdBest || msdBest <0)
                            msdBest = msdTemp;
                            pixelBestHeight = pixelHeight + Delta_Height;
                            pixelBestWidth = pixelWidth + Delta_Width;
                            msdValues{level}(pixelHeight,pixelWidth) = msdBest;
                        end;
                    end;
                end;
            end;
        end;
        if(pixelBestHeight ~=0 && pixelBestWidth ~= 0)
            mask(pixelHeight,pixelWidth) = 2;
            cA_repaint(pixelHeight,pixelWidth,:) = cA{level}(pixelBestHeight,pixelBestWidth,:);
            cV_repaint(pixelHeight,pixelWidth,:) = cV{level}(pixelBestHeight,pixelBestWidth,:);
            cH_repaint(pixelHeight,pixelWidth,:) = cH{level}(pixelBestHeight,pixelBestWidth,:);
            cD_repaint(pixelHeight,pixelWidth,:) = cD{level}(pixelBestHeight,pixelBestWidth,:);
            blockSize = blockSize_begin;                        % Block size for processing
            halfBlockSize = ceil(blockSize/2);		      % Half the size of one block
            searchWindowSize = searchWindowSize_begin;
        else
            blockSize = floor(1.5*blockSize);                        % Block size for processing
            halfBlockSize = floor(1.5*halfBlockSize);		      % Half the size of one block
            searchWindowSize = floor(1.5*searchWindowSize);
        end;
        left=sum(sum(mask==0));
        if(mod(left,100) ==  0)
            if(level~=1)
                reconstructed = idwt2(cA_repaint,cH_repaint,cV_repaint,cD_repaint,'db1',size(cA{level-1}));
                cA{level-1} = reconstructed;
            else
                reconstructed = idwt2(cA_repaint,cH_repaint,cV_repaint,cD_repaint,'db1',size(image));
            end;
            subplot(121),imagesc(uint8(cA_repaint.* mask(1:size(cA_repaint,1),1:size(cA_repaint,2),ones(1,1,3)))),title('Degraded image');
            subplot(122),imagesc(uint8(reconstructed)),title('Inpainted image');
            pause(0.1);
        end;
    end;
    if(level ~=1)
        msdValues{level} = imresize(msdValues{level},(size(cA{level-1},1)/size(cA{level},1)),'nearest');
    end;
    if(level~=1)
        reconstructed = idwt2(cA_repaint,cH_repaint,cV_repaint,cD_repaint,'db1',size(cA{level-1}));
        cA{level-1} = reconstructed;
    else
        reconstructed = idwt2(cA_repaint,cH_repaint,cV_repaint,cD_repaint,'db1',size(image));
    end;
    imagesc(uint8(reconstructed)),title('Inpainted image');
    pause(0.1);
end;
end