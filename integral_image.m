Filename = 'sydney_tiny.png'; % Input file name
blockSize = 16;                        % Block size for processing
halfBlockSize = ceil(blockSize/2);		      % Half the size of one block
Image = double(imread(Filename));
SearchWindowSize = 20;
[imageHeight,imageWidth,C] = size(Image);

%% TASK 2.2
% Detect pixels that need to be inpainted (Mask=0: inpaint, Mask=1: leave untouched)
Mask = zeros(imageHeight,imageWidth);

tol =[180 180 40];  % yellow: high red, high green, low blue

redBand = Image(:,:,1);
T1 = tol(1);
Mask(redBand < T1) = 1;

greenBand = Image(:,:,2);
T2 = tol(2);
Mask(greenBand < T2) = 1;

blueBand = Image(:,:,3);
T3 = tol(3);
Mask(blueBand > T3) = 1;

se = strel('square', 3);
Mask = imerode(Mask, se);
SuperMask = imerode(Mask,strel('square',blockSize+1));

% set the pixels that need to be inpainted to zero
InpaintedImage = Image .* Mask(:,:,ones(1,1,3));

figure,imagesc(uint8(InpaintedImage));

yBand  = 0.299*InpaintedImage(:,:,1) + 0.587*InpaintedImage(:,:,2) + 0.114*InpaintedImage(:,:,3);
rBand = InpaintedImage(:,:,1);
gBand = InpaintedImage(:,:,2);
bBand = InpaintedImage(:,:,3);

figure;

    % HOE HET ALGORITME WERKT:
    % FOR (ALLE VECTOREN v DIE BINNEN HET ZOEKVENSTER PASSEN){
    %   -BEREKEN EEN MATRIX DIE VOOR ELKE PIXEL DE MSD BEREKENT TOV PIXEL+v
    %   -MAAK HIER EEN INTEGRAL IMAGE VAN
    %   -KIJK VOOR ELKE PIXEL DIE IN HET MASK ZIT OF VOOR DEZE VECTOR v DE
    %    MSD-WAARDE OPTIMAAL IS, ZO JA VERVANG DE PIXEL
    % }

bestMSD = -ones(imageHeight,imageWidth);

for Delta_Height = -floor(SearchWindowSize/2):floor(SearchWindowSize/2),
    for Delta_Width = -floor(SearchWindowSize/2):floor(SearchWindowSize/2),       

	    % non-zero displacement
        if (Delta_Height~=0 || Delta_Width~=0),

            % compute the feasible ranges for m and n (to stay within the image boundaries)
            height_min = max(min(halfBlockSize-Delta_Height,imageHeight-halfBlockSize),halfBlockSize+1);
            height_max = min(max(imageHeight-halfBlockSize-1-Delta_Height, halfBlockSize),imageHeight-halfBlockSize-1);
            width_min = max(min(halfBlockSize-Delta_Width, imageWidth-halfBlockSize),halfBlockSize+1);
            width_max = min(max(imageWidth-halfBlockSize-1-Delta_Width, halfBlockSize),imageWidth-halfBlockSize-1);

            if width_min>width_max || height_min>height_max,
                continue;
            end;                                     

            range_height = 1+(height_min:height_max);
            range_width = 1+(width_min:width_max);

            % compute the mean squared difference between the pixel intensities in
            % two displaced images based on an integral image representation
                
            % VOOR ELKE PIXEL IN SUPERMASK DE MSD BEREKENEN TEN OPZICHTE VAN DE PIXEL DIE OP EEN VECTOR [Delta_Height,Delta_Width] ERVANDAAN LIGT
                
            lowestHeight = 1;
            highestHeight = range_height;

            lowestWidth = 1;
            highestWidth = range_width;
              
            translatedYBand = imdilate(yBand,translate(strel(1),[-Delta_Height,-Delta_Width]));
            msd = abs(yBand - translatedYBand).^2;
               
            % INTEGRAL IMAGES BEREKENEN
            intMSD = intimage(msd);
            intMask = intimage(Mask);
                
            % MSD'S VOOR ELK BLOK ROND IN TE PAINTEN PIXEL BEREKENEN EN
            % INDIEN BESTE, PIXEL INPAINTEN
            for i = range_height,
                for j = range_width,
                    if (Mask(i,j) == 0 && Mask(i + Delta_Height, j + Delta_Width) == 1)
                        newMSD = double(intMSD(i + halfBlockSize, j + halfBlockSize) - intMSD(i - halfBlockSize, j + halfBlockSize) - intMSD(i + halfBlockSize, j - halfBlockSize) + intMSD(i - halfBlockSize, j - halfBlockSize))/(intMask(i + halfBlockSize, j + halfBlockSize) - intMask(i - halfBlockSize, j + halfBlockSize) - intMask(i + halfBlockSize, j - halfBlockSize) + intMask(i - halfBlockSize, j - halfBlockSize)).^2;
                        oldMSD = bestMSD(i,j);
                        if (oldMSD > newMSD || oldMSD == -1),
                            InpaintedImage(i,j,:) = Image(i+Delta_Height, j+Delta_Width,:);
                            bestMSD(i,j) = newMSD;
                        end;
                    end;
                end;
            end;

           
            % copy the pixel intensities that were present in the original image
            
        end;
        subplot(121),imagesc(uint8(Image)),title('Degraded image');
        subplot(122),imagesc(uint8(InpaintedImage)),title('Inpainted image');
        pause(0.1);
    end;
    fprintf('\nrow ready %d out of %d\n',Delta_Height,SearchWindowSize);
end;
subplot(121),imagesc(uint8(Image)),title('Degraded image');
subplot(122),imagesc(uint8(InpaintedImage)),title('Inpainted image');
pause(0.1);



