clear
Filename = 'Line_scratch1.png';     % original degraded sample image
Image = double(imread(Filename));
[M,N,C] = size(Image);

yBand  = 0.299*Image(:,:,1) + 0.587*Image(:,:,2) + 0.114*Image(:,:,3);
startImage = yBand;

% wavelet transform
nLevel = 1;             % Number of decompositions
decImage = ndwt2(startImage, nLevel, 'db1'); 
LH = decImage.dec{2};      % Vertical detail

% detection line center pixels
h = 20;
extImage = wextend('2D', 'sym', LH, h);
thresh = 3;
centerImage = ones(M,N);
for i= 1:M,
    for j= 1:N,
        block = extImage(i:i+2*h, j:j+2*h);
        avg =  mean(block);
        avgl = mean(avg(h-1:h));
        avgr = mean(avg(h+2:h+3));
        
        if abs(avgl) > thresh && abs(avgr) > thresh && avgl * avgr < 0,
            centerImage(i, j) = 0;
        end
    end
end

% erode
se = strel('line', 2, 0);
erodImage = imerode(centerImage, se);

maskImage = erodImage;

% apply yellow mask on degraded image
RImage = Image(:,:,1); GImage = Image(:,:,2); BImage = Image(:,:,3); 
RImage(maskImage == 0) = 240;
GImage(maskImage == 0) = 240;
BImage(maskImage == 0) = 40;
maskedImage(:,:,1) = RImage; maskedImage(:,:,2) = GImage; maskedImage(:,:,3) = BImage; 


figure, subplot(1,2,1), imagesc(uint8(startImage)), title('Degraded image');
subplot(1,2,2), imagesc(uint8(maskedImage)), title('Masked image');
